import { Producto } from './producto';

export const PRODUCTOS: Producto[] = [
    {id: 1, name: 'Green Tea', price: 3.11},
    {id: 2, name: 'Strawberries', price: 5},
    {id: 3, name: 'Coffe', price: 11.23}
];
import { Component, OnInit } from '@angular/core';
import { PRODUCTOS } from '../../apiProductos';
import { Producto } from '../../producto';
import { ServicioCarritoService } from '../servicio-carrito.service';
//import { ConsoleReporter } from 'jasmine';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  productos = PRODUCTOS;

  productosEnCarrito = [];

  constructor(
    private servicioCarrito: ServicioCarritoService
  ) {}

  ngOnInit() {}
  
  anadirAlCarrito(productos){
      this.servicioCarrito.anadirProductoEnCarrito(productos);
  }

  borrarCarrito(productos){
      this.servicioCarrito.quitarProductoEnCarrito(productos);
  }










/*
  contadorReset(){
    if(this.contador <= 0){
      this.contador = 0;
    } else {
      console.log('Aqui ahi algo que no funciona :(');
    }
  }

  applyOffers(productos){
    if(productos.id === 1 && this.contador >= 1 && this.productosEnCarrito[productos.id] === productos.id){

    }}*/
  
  





}
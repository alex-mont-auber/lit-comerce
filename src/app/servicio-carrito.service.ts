import { Injectable } from '@angular/core';
import { PRODUCTOS } from '../apiProductos'
import { TouchSequence } from 'selenium-webdriver';
import { ProductosComponent } from './productos/productos.component';

@Injectable({
  providedIn: 'root'
})
export class ServicioCarritoService {

  //productosSeleccionados;
  productosActualmenteEnElCarrito = [];
  contador = 0;
  sumaPrecios = 0;

  constructor() { }

  anadirProductoEnCarrito(productos){
    this.productosActualmenteEnElCarrito.push(productos);
    productos.estaDentroDelCarrito = true;
    this.contador++;
    this.sumaPrecios += productos.price;
    console.log('Producto ', productos.name,' añadido!')
    console.log('Productos actualmente en el carrito: ', this.productosActualmenteEnElCarrito);
    console.log('Contador actual: ', this.contador);
    console.log('Total', this.sumaPrecios);
    return this.sumaPrecios;
  }

  quitarProductoEnCarrito(productos){
    productos.estaDentroDelCarrito = false;
    this.productosActualmenteEnElCarrito.map((productoActual,index) =>{
        if(productoActual.id === productos.id){
          this.productosActualmenteEnElCarrito.splice(index,1);
          this.contador--;
          this.sumaPrecios -= productos.price;
          if(this.contador <= 0 || this.sumaPrecios <= 0){
            this.contador = 0;
            this.sumaPrecios = 0;
          }
        }
    });
    console.log('Producto ', productos.name,' quitado!' )
    console.log('Productos actualmente en el carrito: ', this.productosActualmenteEnElCarrito);
    console.log('Contador actual: ', this.contador);
    console.log('Total', this.sumaPrecios);
    return this.sumaPrecios;
  }

  pasarCarritoAComponentes(){
    return this.productosActualmenteEnElCarrito;
  }

  pasarTotalAComponentes(){
    return this.sumaPrecios;
  }
  
}

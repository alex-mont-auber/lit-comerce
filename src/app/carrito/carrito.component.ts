import { Component, OnInit, Input, HostListener } from '@angular/core';
import { ServicioCarritoService } from '../servicio-carrito.service';
import { ProductosComponent } from '../productos/productos.component';
import { Producto } from 'src/producto';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {

  carrito = [];
  total: number;

  constructor(
    private servicioCarrito: ServicioCarritoService
  ) { }

  ngOnInit() {
    this.carrito = this.servicioCarrito.pasarCarritoAComponentes();
    this.total = this.servicioCarrito.pasarTotalAComponentes();
  }

  ngDoCheck(){
    this.total = this.servicioCarrito.pasarTotalAComponentes();
  }

  verCarrito(){
    console.log('Actualmente en el carrito hay: ',this.carrito);
  }

}
